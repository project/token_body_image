# Token Body Image.

This module add a token to answer the need of extracting an image URL out
of the body field. This is useful in the case you have a page with a single
body field and you want to setup an image in the meta tags in order to improve
the sharing experience in social media or search engines.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/token_body_image).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/token_body_image).

## Table of contents

- Requirements
- Installation
- Configuration
- Features
- Post-Installation
- Maintainers

## Requirements
This module requires the following modules:

- [Token](https://www.drupal.org/project/token)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

## Configuration

No configuration is needed.

## Features

Add a new token [node:body_image_url]

## Post-Installation

- Add Text (formatted, long) field, Create content and add single image
  within this field.
- Install the require modules.
- Use [node:body_image_url] wherever tokens are allowed with a node context like
  in the meta tags.

## Maintainers

- Philippe Joulot - [phjou](https://www.drupal.org/u/phjou)
